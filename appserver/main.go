package main

import (
	"./auth"
	"./middleware"
	"./signed"
	"fmt"
	"log"
	"net/http"
	"time"
)

func handleMain(w http.ResponseWriter, r *http.Request) {
	clm, _ := auth.ExtractClaim(r)
	fmt.Fprintf(w, "hey, %v", clm.Id)
}

func handleLogin(w http.ResponseWriter, r *http.Request) {
	// Extract auth token from URL and validate
	authToken := r.FormValue("auth")
	clmData, err := signed.GetMessage(signed.SignedMessage(authToken))
	if err != nil {
		msg := fmt.Sprintf("invalid auth token (%v)", err)
		http.Error(w, msg, 401)
	}
	clm, err := auth.DecodeClaim(clmData)
	if err != nil {
		msg := fmt.Sprintf("invalid claim (%v)", err)
		http.Error(w, msg, 401)
	}
	if clm.Expired() {
		http.Error(w, "claim expired", 401)
	}
	// Set the the auth cookie and respond
	
	fmt.Printf("Accepted token at %v: \n\n%v\n\n", time.Now(), authToken)
	err = auth.SetAuthCookie(w, clm.Id, time.Now().Add(time.Minute * 15))
	if err != nil {
		msg := fmt.Sprintf("server error setting cookie: (%v)", err)
		http.Error(w, msg, 500)
	}
	fmt.Fprintf(w, "Succeed!\n\n")
}

func handleLogout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{Name: "auth", Value: ""})
	fmt.Fprintf(w, "You're out, bruh!")
}

func handleLoginGen(w http.ResponseWriter, r *http.Request) {
	bailIf := func(e error) {
		if e != nil {
			http.Error(w, "Server error D:", 500)
		}
	}
	clm := auth.Claim{1, time.Now().Add(time.Minute * 15)}
	clmData, err := clm.Encode()
	bailIf(err)
	msg := signed.Message(clmData)
	enc := fmt.Sprintf("http://localhost:3030/login?auth=%v", msg)
	fmt.Printf("Visit\n%v\n to log in.", enc)
	fmt.Fprintf(w, "Sent. Check ur email")
}

func main() {
	http.HandleFunc("/login", handleLogin)
	http.HandleFunc("/logout", handleLogout)
	http.HandleFunc("/tmpsecretlogin", handleLoginGen)
	http.HandleFunc("/", middleware.WithAuth(handleMain))
	fmt.Println("Listening on localhost:3030")
	err := http.ListenAndServeTLS(":3030", "cert.pem", "key.pem", nil)
	log.Fatal(err)
}
