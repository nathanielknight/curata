package middleware

import (
	"log"
	"net/http"
	"testing"
	"time"
	"../authtoken"
)


var pastDate = time.Date(2001, time.February, 17, 03, 30, 31, 0, time.UTC)
var validClaims = []authtoken.Claim{
	authtoken.Claim{0, time.Now().Add(cookieTimeout)},
	authtoken.Claim{456789123, time.Now().Add(cookieTimeout)},
}

var validTokens []authtoken.Token // constructed in init()
var invalidTokens = []authtoken.Token{
	authtoken.Token{authtoken.Claim{0, time.Now()}, []byte("")},
	authtoken.Token{authtoken.Claim{0, pastDate}, []byte("")},
	authtoken.Token{authtoken.Claim{0, time.Now()}, []byte("asdfjklsemicolon")},
}

func init() {
	// Construct valid tokens
	for _, c := range validClaims {
		t, err := authtoken.NewToken(c)
		if err != nil {
			log.Fatal(err)
		}
		validTokens = append(validTokens, *t)
	}

	// Construct invalid tokens
	t, err := authtoken.NewToken(authtoken.Claim{0, pastDate})
	if err != nil {
		log.Fatal(err)
	}
	invalidTokens = append(invalidTokens, *t)
}


func attachToken(r *http.Request, tk authtoken.Token) error {
	c, err := authCookie(tk)
	if err != nil {
		return err
	}
	r.AddCookie(c)
	return nil
}


func TestAuthenticateRequest(t *testing.T) {
	for _, tk := range validTokens {
		if !tk.Valid() {
			t.Fatal("Test error: expected to be valid: ", tk)
		}
		r, err := http.NewRequest("GET", "localhost", nil)
		if err != nil {
			t.Error(err)
		}
		err = attachToken(r, tk)
		if err != nil {
			t.Error(err)
		}
		auth, err := authenticateRequest(r)
		if !auth || err != nil {
			t.Error(err)
		}
	}
	for _, tk := range invalidTokens {
		r, err := http.NewRequest("GET", "localhost", nil)
		if err != nil {
			t.Error(err)
		}
		err = attachToken(r, tk)
		if err != nil {
			t.Error(err)
		}
		b, err := authenticateRequest(r)
		if b {
			t.Error(err)
		}
	}
}



// TODO(nknight)
// Integration Tests. Vary on
//   - presence of auth cookie
//   - validity of auth cookie
