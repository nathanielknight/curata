package middleware

import (
	"fmt"
	"errors"
	"net/http"
	"time"
	"../auth"
)

var cookieTimeout = 15 * time.Minute


func authenticateRequest(r *http.Request) (*auth.Claim, error) {
	clm, err := auth.ExtractClaim(r)
	if err != nil {
		return nil, err
	}
	if clm == nil {
		return nil, errors.New("Nil claim!")
	}
	if clm.Expired() {
		msg := fmt.Sprintf("token expired (%v < %v)", clm.Expires, time.Now())
		return nil, errors.New(msg)
	}
	return clm, nil
}


func WithAuth(f http.HandlerFunc) http.HandlerFunc {
	fmt.Println("Decorating...\n")
	return func(w http.ResponseWriter, r *http.Request) {
		_, err := authenticateRequest(r)
		if err == nil {
			f(w, r)
		} else {
			// TODO(nknight): add a www-authenticate header
			msg := fmt.Sprintf("Not logged in (%v)", err)
			http.Error(w, msg, 401)
		}
	}
}
