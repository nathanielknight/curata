package signed

// This package implements an encoding and HMAC system. Given a
// `[]byte` it gives back a string wich contains
//
//    base64.RawURLEncoding.EncodeToString(msg) + "." +
//    base64.RawURLEncoding.EncodeToString(HMAC of msg)
//
// This conflates encoding and signging, but makes the signed object
// suitable for inclusionin URLs, Cookies, etc. without managing a
// separate encoding system.

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"strings"
)

var macKey []byte
var keySize = 128 / 8 // 128 bit key for sha256

func init() {
	// TODO(nknight): generate a strong key
	macKey = []byte("asdfjkl")
}

func getMAC(b []byte) []byte {
	mac := hmac.New(sha256.New, macKey)
	mac.Write(b)
	return mac.Sum(nil)
}

// An incoming unsigned, unencoded message
type unsignedMessage []byte

// An encoded message with its signatured
type SignedMessage string

func (m unsignedMessage) signed() SignedMessage {
	encodedMsg := base64.RawURLEncoding.EncodeToString([]byte(m))
	mac := getMAC(m)
	encodedMAC := base64.RawURLEncoding.EncodeToString(mac)
	msg := strings.Join([]string{encodedMsg, encodedMAC}, ".")
	return SignedMessage(msg)
}

func (m SignedMessage) valid() bool {
	parts := strings.SplitN(string(m), ".", 2)
	if len(parts) != 2 {
		return false
	}
	msg, err := base64.RawURLEncoding.DecodeString(parts[0])
	if err != nil {
		return false
	}
	mac, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return false
	}
	expectedMAC := getMAC(msg)
	return hmac.Equal(mac, expectedMAC)
}

// This is the interface for creating a signed object. It should be
// used like:
//
//     msg := dataObject.Serialize()
//     signedMsg := signed.Message(msg)
func Message(b []byte) SignedMessage {
	return unsignedMessage(b).signed()
}

// This is the interface for verifying and extracting signed
// messages. It should be used like:
//
//     msg, err := GetMessage(signedMessage)
//     if err != nil {
//         // handle auth or extraction errors
//     } else {
//         // use the validated message
//     }
func GetMessage(m SignedMessage) ([]byte, error) {
	if !m.valid() {
		return nil, fmt.Errorf("message signature invalid")
	} else {
		parts := strings.SplitN(string(m), ".", 2)
		msg, err := base64.RawURLEncoding.DecodeString(parts[0])
		return msg, err
	}
}
