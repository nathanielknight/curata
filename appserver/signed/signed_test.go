package signed

import (
	"encoding/base64"
	"strings"
	"testing"
	"testing/quick"
)

var badSig = base64.RawURLEncoding.EncodeToString([]byte("asdfjkl"))

func TestSignVerify(t *testing.T) {
	f := func(b []byte) bool {
		msg := unsignedMessage(b).signed()
		return msg.valid()
	}
	quick.Check(f, nil)
}

func TestPublicSignVerify(t *testing.T) {
	f := func(b []byte) bool {
		msg := Message(b)
		if !msg.valid() {
			return false
		}
		payload, err := GetMessage(msg)
		if err != nil {
			t.Errorf("error while getting message: %v", err)
			return false
		}
		if len(payload) != len(b) {
			t.Errorf("message changed after serialize/deserialize")
			return false
		}
		for i, x := range b {
			if payload[i] != x {
				t.Errorf("message changed at position %v", i)
				return false
			}
		}
		return true
	}
	quick.Check(f, nil)
}

func TestSignNoVerify(t *testing.T) {
	f := func(msg string, fakeMAC string) bool {
		fakeSignedMessage := SignedMessage(strings.Join([]string{msg, fakeMAC}, "."))
		return !fakeSignedMessage.valid()
	}
	quick.Check(f, nil)
}

var cases = []string{"", "asdjfkl"}

func TestSpecificCases(t *testing.T) {
	for _, c := range cases {
		realSigned := Message([]byte(c))
		fakeSigned := SignedMessage(strings.Join([]string{c, badSig}, "."))
		if !realSigned.valid() {
			t.Errorf("supposed to be valid: c = %v , m = %v", c, realSigned)
		}
		if fakeSigned.valid() {
			t.Errorf("supposed to be invalid: c = %v , m = %v", c, fakeSigned)
		}
	}
}

func TestExtractAndVerify(t *testing.T) {
	f := func(b []byte) bool {
		m := Message(b)
		e, err := GetMessage(m)
		if err != nil {
			return false
		}
		if len(b) != len(e) {
			return false
		}
		for i, x := range b {
			if e[i] != x {
				return false
			}
		}
		return true
	}
	quick.Check(f, nil)
}

func TestExtractNoVerify(t *testing.T) {
	f := func(s string) bool {
		m := strings.Join([]string{s, badSig}, ".")
		msg, err := GetMessage(SignedMessage(m))
		if err == nil {
			return false
		}
		if msg != nil {
			return false
		}
		return true
	}
	quick.Check(f, nil)
}
