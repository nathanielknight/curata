package auth

import (
	"math/rand"
	"reflect"
	"testing"
	"testing/quick"
	"time"
)

var pastTime = time.Date(2005, time.February, 16, 3, 15, 0, 0, time.UTC)
var futureTime = time.Now().Add(15 * time.Minute)

func check(t *testing.T, e error) {
	if e != nil {
		t.Errorf("%v", e)
	}
}

func TestUnexpiredClaim(t *testing.T) {
	c1 := Claim{1, futureTime}
	if c1.Expired() {
		t.Errorf("Expected to be unexpired: %v", c1)
	}
	enc, err := c1.Encode()
	check(t, err)
	c2, err := DecodeClaim(enc)
	check(t, err)
	if c2.Expired() {
		t.Errorf("Expected to be unexpired: %v", c2)
	}
	if !reflect.DeepEqual(c1, *c2) {
		t.Errorf("Expected to be equal: %v != %v", c1, *c2)
	}
}

func TestExpiredClaim(t *testing.T) {
	c1 := Claim{1, pastTime}
	if !c1.Expired() {
		t.Errorf("Expected to be expired: %v", c1)
	}
	enc, err := c1.Encode()
	check(t, err)
	c2, err := DecodeClaim(enc)
	check(t, err)
	if !c2.Expired() {
		t.Errorf("Expected to be expired: %v", c2)
	}
	if !reflect.DeepEqual(c1, *c2) {
		t.Errorf("Expected to be equal: %v != %v", c1, *c2)
	}
}



func testEncodeDecode(t *testing.T, dHour, dMinute int) bool {
	expires := time.Now().
		Add(time.Duration(dHour) * time.Hour).
		Add(time.Duration(dMinute) * time.Minute)
	clm := Claim{3, expires}
	enc, err := clm.Encode()
	if err != nil {
		t.Fatalf("error encoding '%v': %v", clm, err)
		return false
	}
	dec, err := DecodeClaim(enc)
	if err != nil {
		t.Fatalf("error decoding: %v", err)
		return false
	}
	if !reflect.DeepEqual(clm, *dec) {
		t.Fatalf("Claims don't match\nclm: %v\ndec: %v\n", clm, *dec)
		return false
	}
	return true
}

func TestEncodeDecode(t *testing.T) {
	f := func(r *rand.Rand) bool {
		dHour := 72 - r.Intn(144)
		dMinute := 60 - r.Intn(120)
		return testEncodeDecode(t, dHour, dMinute)
	}
	quick.Check(f, nil)
}
