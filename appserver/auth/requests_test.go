package auth

import (
	"../model"
	"net/http"
	"testing"
	"time"
)

// This test
//   - adds an auth token to an HTTP Request as a cookie
//   - extracts the token and verifies it
func TestAddExtractAuthToken(t *testing.T) {
	uid := model.UserId(3)
	expires := time.Now().Add(time.Minute * 15)
	ck, err := authCookie(uid, expires)
	if err != nil {
		t.Fatalf("error constructing cookie: %v", err)
	}
	r, _ := http.NewRequest("GET", "localhost", nil)
	r.AddCookie(ck)
	clm, err := ExtractClaim(r)
	if err != nil {
		t.Fatalf("error extracting claim: %v", err)
	}
	idEqual := (uid == clm.Id)
	expEqual := (expires == clm.Expires)
	if !(idEqual && expEqual) {
		t.Fatalf("expected claims to be equal: %v %v | %v", uid, expires, *clm)
	}
}

// TODO(nknight): Integration tests for auth cookies (http Client and
// Server)
