package auth

import (
	"../model"
	"../signed"
	"fmt"
	"net/http"
	"time"
)

const authCookieName = "auth"

// Construct a net/http Cookie containing a signed auth Claim for the
// given user expiring at the given time.
func authCookie(uid model.UserId, expires time.Time) (*http.Cookie, error) {
	clm := Claim{uid, expires}
	val, err := clm.Encode()
	signedVal := signed.Message(val)
	if err != nil {
		fmt.Errorf("encoding claim: %v", err)
	}
	ck := http.Cookie{
		Name:   authCookieName,
		Value:  string(signedVal),
		MaxAge: 3600,
		Secure: true,
	}
	return &ck, nil
}

// Add a Set-Cookie header containing an auth Cookie with the given
// User Id and expiry time to an http ResponseWriter.
func SetAuthCookie(w http.ResponseWriter, uid model.UserId, expires time.Time) error {
	ck, err := authCookie(uid, expires)
	if err != nil {
		return err
	}
	http.SetCookie(w, ck)
	return nil
}

// Retrieve, verify, and decode the claim stored in an auth Cookie.
func ExtractClaim(r *http.Request) (*Claim, error) {
	// get the auth token cookie
	authCookie, err := r.Cookie(authCookieName)
	if err != nil {
		return nil, fmt.Errorf("extracting claim: %v", err)
	}
	// verify the signature
	msg, err := signed.GetMessage(signed.SignedMessage(authCookie.Value))
	if err != nil {
		return nil, fmt.Errorf("signature invalid: %v", authCookie)
	}
	// decode the claim
	clm, err := DecodeClaim(msg)
	if err != nil {
		return nil, err
	}
	return clm, nil
}
