package auth

import (
	"../model"
	"encoding/json"
	"fmt"
	"time"
)

// A claim that a request comes from the with the given Id. If a
// request has a signed one of these, we grant it access to privileged
// operations.
type Claim struct {
	Id      model.UserId
	Expires time.Time
}

func (c Claim) Expired() bool {
	return c.Expires.Before(time.Now())
}

// Serialize a claim (into JSON) and return it as a []byte.
func (c Claim) Encode() ([]byte, error) {
	jBytes, err := json.Marshal(c)
	if err != nil {
		return nil, fmt.Errorf("while serializing Claim to json: %v", err)
	} else {
		return jBytes, nil
	}
}

// Deserialize a claim (from Base64, then JSON), and return a pointer
// to it.
func DecodeClaim(msg []byte) (*Claim, error) {
	var c Claim
	err := json.Unmarshal([]byte(msg), &c)
	if err != nil {
		return nil, fmt.Errorf("decoding JONS: %v", err)
	}
	return &c, nil
}
