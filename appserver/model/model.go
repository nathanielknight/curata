package model

import (
	"time"
)



type UserId uint64

type User struct {
	Id UserId
	Email string
	PasswordHash []byte
	PasswordSalt []byte
}



type CuratumId uint64

type Curatum struct {
	Id CuratumId
	User UserId
	Title string
	Link string
	Description string
	Added time.Time
}
