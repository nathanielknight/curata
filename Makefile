deps:
	go get github.com/mattn/go-sqlite3
	go install github.com/mattn/go-sqlite3
	go get golang.org/x/crypto/bcrypt
	go install golang.org/x/crypto/bcrypt

dev-certs: cert.pem key.pem
	go run ${GOROOT}/src/crypto/tls/generate_cert.go --host dev.localhost

test:
	(cd appserver; go test)

clean:
	rm cert.pem key.pem curata.db


